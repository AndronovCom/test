<?php 
	//подключение классов
	function autoload($class)
	{
		require_once ('lib/'.strtolower($class).'.php');
	}
	//Регистрирует заданную функцию в качестве реализации метода __autoload()
	spl_autoload_register('autoload');
  require_once 'lib/functions.php';
 // redirect('lib/login.php');
		
	$db=new PDOConfig();
  $auth=new Auth($db,new Session([
    'save_path'=>__DIR__.DIRECTORY_SEPARATOR.'storage',
    'cookie_secure'=>false
    ,'cookie_domain'=>'learn/FormOop/'
    ]),
  [
    'domen'=>'learn/FormOop/',
    'secure'=>false]);

	//регистрация пользователя
	if (($_SERVER['REQUEST_METHOD']=='POST')&&isset($_POST['btnRegistr']))
	{
		$reg= new Reg($_POST['InputLogin'],$_POST['InputPassword1'],$_POST['InputPassword2'],$_POST['InputEmail'],$_POST['InputSurname'],$_POST['InputName'],$_POST['InputPhone']);
		$result=$db->fetch('SELECT login FROM users WHERE login=:InputLogin;',[':InputLogin'=>$reg->getLogin()]);
		$reg->unique((array)$result,'login','Логин занят');
		$result=$db->fetch('SELECT email FROM users WHERE email=:InputEmail;',[':InputEmail'=>$reg->getEmail()]);
		$reg->unique((array)$result,'email','Email занят');
		$reg->equality($reg->getPassword(),$reg->getConfirm_password(),'password','Пароли не совпадают');
		$reg->regex(Reg::LOGIN_PATTERN,$reg->getLogin(),'login','Введите корректный логин');
		$reg->regex(Reg::H_PASSWORD_PATTERN,$reg->getPassword(),'password','Введите корректный пароль');
		if(empty($reg->getErrors()))
		{
			$reg->generateHash();
			$result=$db->executePrepare('INSERT INTO users ( id,login, password, email, surname, name, phone) VALUES (null,:InputLogin, :InputPassword1,:InputEmail, :InputSurname, :InputName, :InputPhone)', [
				':InputLogin'=>$reg->getLogin(),
				':InputPassword1'=>$reg->getPassword(),
				':InputEmail'=>$reg->getEmail(),
				':InputSurname'=>$reg->getSurname(),
				':InputName'=>$reg->getName(),
				':InputPhone'=>$reg->getPhone()
			]);
			unset($_POST);

      $_POST['error']='Регистрация прошла успешно';
		}
	}

//авторизация	
  if (($_SERVER['REQUEST_METHOD']=='POST') && isset($_POST['btnEnter'])) {
      if($auth->attemp(clean($_POST['InputLogin']), $_POST['InputPassword'], isset($_POST['check'])))
      {
        header ('Location: main.php');
      }
      else{
        $_POST['error']='Введен неверно логин или пароль';
      }
    }

//восттановление пароля
   if (($_SERVER['REQUEST_METHOD']=='POST') && isset($_POST['btnRecov'])) {
   	$auth->restorePass($_POST['InputEmail']);
   }

//логаут
  if (!$auth->check()) {
    $auth->logout();
    }
  require 'index.html';
	 ?>
