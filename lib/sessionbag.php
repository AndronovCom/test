<?php 
	class SessionBag implements ArrayAccess, Iterator, Countable
	{
		protected $elements=[];

		public function __construct($elements=[])
		{
			$this->elements=$elements;			
		}

		//преобразование массива
		public function toArray():array
		{
			$array=[];
			foreach ($this->elements as $key => $value) {
				if ($value instanceof static) {
					$array[$key]=$value->toArray();
					continue;
				}
				$array[$key]=$value;
			}
			return $array;
		}

		
		//для вызова функцией var_dump
		public function __debugInfo()
		{
			return $this->toArray();
		}

		//очистка массива
		public function clear():SessionBag
		{
			$this->elements=[];
			return $this;
		}

		//
		public function link(array &$link):SessionBag
		{
			$this->elements=&$link;
			return $this;
		}

	    /*методы Iterator*/
	    //возврат текущего элемента
	    public function current()
	    {
	    	return current($this->elements);
	    }

	    //переход к следующему элементу
	 	public function next()
	    {
	    	next($this->elements);
	    }

	    //возврат ключа текущего элемента-Iterator
	    public function key()
	    {
	    	key($this->elements);
	    }

	    //перемотка итератора на перввый элемент
	    public function rewind()
	    {
	    	reset($this->elements);
	    }

	    //проверяет корректность текущей позиции-Iterator
	     public function valid()
	    {
	    	return $this->key()!==null && $this->key()!==false;
	    }

		/*методы ArrayAccess*/
		//присваивает значение заданному смещению
	     public function offsetSet($offset, $value)
	    {
	    	if (is_array($value)) {
	    		$offset===null?$this->elements[]=new static($value):$this->elements[$offset]=new static($value);
	    		return;
	    	}
	    	$offset===null?$this->elements[]=$value:$this->elements[$offset]=$value;
	    }

	    //возвращает заданное смещение(ключ)
	     public function offsetGet($offset)
	    {
	    	if (!isset($this->elements[$offset])) {
	    		$this->elements[$offset]=new static();
	    	}
	    	return $this->elements[$offset];
	    }

	    //удаляет смещение
	     public function offsetUnset($offset)
	    {
	    	unset($this->elements[$offset]);
	    }

	    //определяет, существует ли заданное смещение(ключ)
	     public function offsetExists($offset)
	    {
	    	if (isset($this->elements[$offset]))
	    	{
	    		if ($this->elements[$offset]instanceof static)
	    		{
	    			if ($this->elements[$offset]->count()>0)
	    			{
	    				return true;
	    			}
	    			else 
	    			{
	    				return false;
	    			}
	    		}
	    		else 
	    		{
	    			return true;
	    		}
	    	}
	    	else 
	    	{
	    		return false;
	    	}
	    }

		/*методы Countable*/
	    //возвращает кол-во элементов объекта-Countable
	      public function count()
	    {
	    	return count($this->elements);
	    }	
	}
