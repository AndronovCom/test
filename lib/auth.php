<?php 
	class Auth
	{
		protected $pdo;
		protected $session;
		protected $user;
		protected $cookieParams=[];
		protected $key_user;

		public function __construct(PDOConfig $pdo, session $session, array $cookieParams=[])
		{
			$this->pdo=$pdo;
			$this->session=$session;
			$this->setCookieParams($cookieParams);
			$this->key_user = !$this->keyExists('key_user')?:$_COOKIE['key_user'];
			if (!$this->session->isStarted()) {
				$this->session->start();
			}
			

			$this->check() ? $this->user=$this->findUser(['id'=>$this->session['id']]):$this->isRemember();
		}

		public function setCookieParams(array $params):Auth
		{
			if (isset($params['expire'])) {
				$this->cookieParams['expire']=$params['expire'];
			}
			else{
				$this->cookieParams['expire']=60*60*24*365;
			}
			$this->cookieParams['path']= isset($params['path'])?$params['path']:'/';
			if (!isset($params['domen'])) {
				throw new RuntimeException('Параметр domen обязателен');
			}
			else{
				$this->cookieParams['domen']=$params['domen'];
			}
			$this->cookieParams['secure']=isset($params['secure'])?$params['secure']:true;
			$this->cookieParams['httponly']=isset($params['httponly'])?$params['httponly']:true;
			return $this;
		}

		public function getPdo():PDOConfig
		{
			return $this->pdo;
		}

		public function getSession():Session
		{
			return $this->session;
		}

		public function getUser():Reg
		{
			if (!$this->user instanceof Reg) {
				throw new RuntimeException('Нет аутентифицированного пользователя');
				
			}
			return $this->user;
		}

		public function login(Reg $user, bool $remember=false):Auth
		{
			if ($user->getId()===null) {
				throw new RuntimeException('Идентификатор пользователя не установлен');
			}
			$this->session->regenerate();
			$this->session['id']=$user->getId();
			$this->user=$user;
			if ($remember) {

				$key_user=hash('sha512',time().random_bytes(15).microtime());
				if ($this->keyExists('key_user')) 
				{
					$sth=$this->pdo->executePrepare('UPDATE users_keys SET key_user = ? WHERE key_user = ?',[$key_user,$this->key_user]);
				}
				else{
					$sth=$this->pdo->executePrepare('INSERT INTO users_keys VALUES (null, :userId, :key) ',[':userId'=>$user->getId(), ':key'=>$key_user]);
				}
				setcookie('key_user',$key_user,time()+$this->cookieParams['expire']);
			}
			return $this;
		}

		public function logout()
		{
			unset($this->session['id']);
			unset($this->user);
//проверить
			if ($this->keyExists('key_user')) {
				$sth=$this->pdo->executePrepare('DELETE FROM users_keys WHERE key_user = :key LIMIT 1',[':key'=>$this->key_user]);
				setcookie('key_user','',time()-1);
			}

		}

		//проверка идентификатора сессии
		public function check():bool
		{
			return isset($this->session['id']);
		}

		public function isRemember()
		{
			if ($this->keyExists('key_user')) {

					$sth=$this->pdo->fetch('SELECT user_id FROM users_keys WHERE key_user=:key', [':key'=>$this->key_user]);

				$this->login($this->findUser(['id'=>$sth['user_id']]), true);

			}
		}

		protected function keyExists($value):bool{
			return isset($_COOKIE[$value]);
		}

		protected function findUser(array $data):Reg{
			$sql="SELECT * FROM users WHERE ";
			$bindings=[];

			foreach ($data as $key => $value) {
				$sql.=$key." = :".$key;
				$value===end($data)?:$sql.=' AND ';
				$bindings[':'.$key]=$value;
				
			}

			$user=$this->pdo->fetch($sql, $bindings);

			if ($user===false) {
				throw new UserNotFoundException('Не удалось найти пользователя');
			}

			return($user=new Reg($user['login'], $user['password'], $user['password'], $user['email'], $user['surname'], $user['name'], $user['phone'], $user['id']));
		}

		//проверка пользователя
		public function attemp(string $login, string $password, bool $remember=false):bool
		{
			$user;
			try {
				$user=$this->findUser(['login'=>$login]);
				}
			catch (UserNotFoundException $exception) {
				return false;
			}
			
			if(password_verify($password, $user->getPassword()))
			{
				$this->login($user, $remember);
				$this->checkSession(true);
				return true;
			}
			return false;
		}

		//запись в сессию индикатора авторизации
		    public function checkSession($status=false)
		    {
		    	$this->session['status']=$status;
		    }

		 //восстановление пароля
		 public function restorePass($email)
		 {
		 	$user=$this->pdo->fetch('SELECT * FROM users WHERE email=:email', [':email'=>$email]);
		 	if ($user===false) {
				throw new UserNotFoundException('Не удалось найти пользователя');
			}
			else{
				$this->user=new Reg($user['login'], $user['password'], $user['password'], $user['email'], $user['surname'], $user['name'], $user['phone'], $user['id']);
				$pass=$this->user->generateSalt(15);
				$this->user->setPass($pass);
				$this->user->generateHash();
				$sth=$this->pdo->executePrepare('UPDATE users SET password = ? WHERE id = ?',[$this->user->getPassword(),$this->user->getId()]);
				$message='Логин: '.$this->user->getLogin(). "\nПароль: " .$pass;
				$this->sendMessage($this->user->getEmail(), 'Восстановление пароля', $message);
				unset($_POST);
				$_POST['error']='Письмо с новым паролем отправлено Вам на почту';
			}
		 }

		 //отправка письма пользователю
		 public function sendMessage($email, $theme, $message)
		 {
		 	mail($email, $theme, $message);
		 }
//отправляем запрос на поиск имеыла
//если он вернул строку, то меняем пароль и перезаписываем в бд
//отправить пароль на ящик
	}