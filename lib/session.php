<?php 
		class Session implements ArrayAccess, IteratorAggregate, Countable
		{
			protected $sessionBag;
			protected $options=[];
			protected $start=false;
			protected $id;
			protected $name;

			function __construct(array $options=[])
			{
					$this->setOptions($options);
				$this->sessionBag=new SessionBag();
			}

		    public function getId()
			{
				return $this->id;
			}

			public function setId($id):Session
			{
				$this->id=$id;
				return $this;
			}

			public function getName()
			{
				return $this->name;
			}

			public function setName($name):Session
			{
				$this->name=$name;
				return $this;
			}

			public function setOptions(array $options):Session
			{
				if ($this->isStarted()) {
					throw new RuntimeException('Сессия уже запущена, невозможно установить опции');
				}
				$valid=array_flip([
					'save_handler', 'save_path', 'name', 'auto_start', 'serialize_handler', 'gc_probability', 'gc_divisor','gc_maxlifetime','referer_check', 'use_strict_mode','use_cookies','use_only_cookies', 'cookie_lifetime', 'cookie_path', 'cookie_domain', 'cookie_secure', 'cookie_httponly', 'cache_limiter','cache_expire','use_trans_sid','trans_sid_tags', 'trans_sid_hosts', 'sid_length','sid_bits_per_character', 'lazy_write','read_and_close']);
				$options=array_merge([
					'use_strict_mode'=>true,
					'use_trans_sid'=>'off',
					'cache_limiter'=>'nocache',
					'sid_length'=>48,
					'sid_bit_per_character'=>6
					,'use_only_cookies'=>true,
					'cookie_httponly'=>true,
					'cookie_lifetime'=>0], $options);

				foreach ($options as $option => $value) {
					if (isset($valid[$option])) {
						$this->options[$option]=$value;
					}
				}
				return $this;
			}

			public function start():bool
			{
				if ($this->isStarted()) {
					$this->isStarted();
				}
				if (session_status()===PHP_SESSION_ACTIVE) {
					throw new RuntimeException('Невозможно запустить сессию. Она уже запущена из PHP');
				}
				if (headers_sent()!==false) {
					throw new RuntimeException('Невозможно запустить сессию. Были отправлены заголовки');
				}
				if ($this->id!==null) {
					session_id($this->id);
				}
				if ($this->name!==null) {
					session_name($this->name);
				}

				$this->start=session_start($this->options);
				if (!$this->isStarted()) {
					throw new RuntimeException('Не удалось запустить сессию');
				}
				$this->sessionBag=$this->sessionBag-> link($_SESSION); 

				return $this->isStarted();
			}

			//уменьшение риска атаки фиксации сессии
			public function regenerate():Session
			{
				if (session_regenerate_id()===false) {
					throw new RuntimeException('Не удалось сгенерировать новый сессионный id');
				}
				$this->id=session_id();
				return $this;
			}

			public function close()
			{
				session_vrite_close();
				$this->start=false;
				$this->sessionBag=new SessionBag();
			}

			public function isStarted():bool
			{
				return $this->start;
			}

			//преобразование массива
			public function toArray():array
			{
				return $this->sessionBag->toArray();
			}

			public function clear():Session
			{
				$this->sessionBag->clear();
				return $this;
			}

			/*методы ArrayAccess*/
			//присваивает значение заданному смещению
		     public function offsetSet($offset, $value)
		    {
		    	$this->sessionBag->offsetSet($offset, $value);
		    }

		    //возвращает заданное смещение(ключ)
		     public function offsetGet($offset)
		    {
		    	return $this->sessionBag->offsetGet($offset);
		    }

		    //удаляет смещение
		     public function offsetUnset($offset)
		    {
		    	$this->sessionBag->offsetUnset($offset);
		    }

		    //определяет, существует ли заданное смещение(ключ)
		     public function offsetExists($offset)
		    {
		    	$this->sessionBag->offsetExists($offset);
		    }

			/*методы Countable*/
		    //возвращает кол-во элементов объекта-Countable
		      public function count()
		    {
		    	return $this->sessionBag->count();
		    	
		    }	

			/*методы IteratorAggregate*/
			//создает новый итератор для экземпляра ArrayObject
		    public function getIterator($value='')
		    {
		    	return $this->sessionBag;
		    }

		    
	}	
