<?php 
	class Reg extends User {
		public $confirm_password;
		private $err=[];
		
		const H_PASSWORD_PATTERN='/^(?=^.{8,}$)((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Za-z]).*$/';
		const LOGIN_PATTERN='/^[a-zA-Z0-9]{4,20}$/';

		public function __construct($login, $password, $confirm_password, $email, $surname, $name, $phone, $id=0)
		{
			$this->login=clean($login);
			$this->password=$password;
			$this->confirm_password=$confirm_password;
			$this->email=clean($email);
			$this->surname=clean($surname);
			$this->name=clean($name);
			$this->phone=clean($phone);
			$this->id=$id;
		}

		public function getLogin()
		{
			return $this->login;
		}

		public function getEmail()
		{
			return $this->email;
		}

		public function getPassword()
		{
			return $this->password;
		}

		public function getConfirm_password()
		{
			return $this->confirm_password;
		}

		public function getSurname()
		{
			return $this->surname;
		}

		public function getName()
		{
			return $this->name;
		}

		public function getPhone()
		{
			return $this->phone;
		}

		public function getId()
		{
			return $this->id;
		}

		public function setId($id)
		{
			$this->id=$id;
		}

		public function setPass($pass)
		{
			$this->password=$pass;
			$this->confirm_password=$pass;
		} 

		//метод для получения ошибок
		public function getErrors()
		{
			return $this->err;
		}

		//метод проверки ошибок
		public function typeError($key)
		{
			if (array_key_exists($key, $this->err))
			{
				return "<div class='error'>".$this->err[$key]."</div>";
			}
		}

		//валидация формы
		public function regex($pattern, $fild,$key, $err)
		{
			preg_match($pattern, $fild)?:$this->err[$key]=$err;
		}

		//проверка корректной длины
		public function len($min, $max, $variable, $key, $err)
		{
			!(strlen($variable)>$max || strlen($variable)<$min)?:$this->err[$key]=$err;
		}
	

		//проверка равенства
		public function equality($password,$confirm_password,$key,$err)
		{
			$password==$confirm_password?:$this->err[$key]=$err;
		}

		//проверка уникальности полей в БД
		public function unique($row,$key, $err)
		{
			!array_key_exists($key, $row)?:$this->err[$key]=$err;
		}


		//генерация соли для шифрования
		public function generateSalt($int)
		{
			$chars='wertyuiopasdfghjklzxcvbnm7896544123QWERTYUIOPASDFGHJKLZXCVBNM06';
			$size=strlen($chars)-1;
			$salt='';
			while ($int--) {
				$salt.=$chars[rand(0, $size)];
			}
			return $salt;
		}

		//шифрование пароля
		public function generateHash($algo=PASSWORD_DEFAULT, array $options=null)
		{
			!is_null($options)?:$options=[
				'salt'=>$this->generateSalt(22),
				'cost'=>10
			];
			$this->password=password_hash($this->password, $algo, $options);
		}
	}
 ?>