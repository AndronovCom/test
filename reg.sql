-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1:3306
-- Время создания: Сен 14 2018 г., 18:52
-- Версия сервера: 5.6.38
-- Версия PHP: 7.2.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `reg`
--

-- --------------------------------------------------------

--
-- Структура таблицы `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `login` varchar(60) NOT NULL,
  `password` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `surname` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `phone` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `users`
--

INSERT INTO `users` (`id`, `login`, `password`, `email`, `surname`, `name`, `phone`) VALUES
(11, 'gfdSShgfd55453HGJ', '$2y$10$P2NkeTZOSSxiUCRMNC1tPuk1Dqjae67SexVgtIk5Gh5UwSTKuS96a', 'gfhfdgh@fsfsd.com', '', '', ''),
(12, 'gfdhssgfd55453HGJ', '$2y$10$OVM0KHR7PCo/VXZZIj5nNOJIMcoZZi8pMmLN8oWfihJfZBwMfBmm.', 'gfasdshfdgh@fsfsd.com', '', '', ''),
(13, 'KIJIJ1545NJHs', '$2y$10$TzxBTll6XHxQWXt4JEVVK.f.abCgAeceLKR4Sprb5QiB/P8Kzarbu', 'gfhfdscgh@fsfscsd.com', '', '', ''),
(14, 'gfdSShgfd55453HGJsd', '$2y$10$M099REs5cjdaQ1YtdCV1K.Kj3c.1uwpZTn8kR9bJIqEk8VEi0d.22', 'dsfdssd@dsf.com', '', '', '');

-- --------------------------------------------------------

--
-- Структура таблицы `users_keys`
--

CREATE TABLE `users_keys` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `key_user` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `users_keys`
--

INSERT INTO `users_keys` (`id`, `user_id`, `key_user`) VALUES
(20, 11, '011f6ea2e4832db89572f45fc3972ba9e3fb3ad16de6520218be11d843a881b4c5583c4b538ff8b9f4450c10945721522a03103d179da021215a541a1c089436'),
(21, 11, 'f4f7b6e2fe13741579b8a755088b097bb8c0be70d4695a1c1cd6434065487badf3d0604a644e70b25e4c6f23037abbc98c0e9194708e0a4c87add779265ec994'),
(22, 12, 'a83651827213cff00648a5f5ed7580c66390c3dd8904002993daeb2a9b6632f46debff2674d097c80841555ef247f85ef5bb3835182a7d0f4d6c2de4687f355a'),
(23, 14, '74aed82554dd5ee385b594bdeab69e9edd602bf664c718ec4d060e468a03a90dca28068dd70c09a2463d62cb2b5ea174c44ac5e432c24a82f295e001d1e1810a'),
(24, 13, 'afc18ac0c6a94fee82e2e45ac202fe6279a87d3a5f66c8dc61485db429bb9944f6db3e982434e45e40b82892af2ab9cc371775a3e567eeaec281806f2767b63e'),
(25, 14, '6114a1f718f68e547130bfc652d37571c4b81e021efc4d15eeee42e70ea9931d9936e2d690085530b346ef2cc9ee50a8527b4487c176a5ee42711a1ba5a40cba'),
(26, 13, '8379b5509032c9b84597a7737c00f14a78a17fb4a1d912ba84d0bc66c0ae153a610d6f88ffbdaa6bae84f70e5be296547e8610932501f9254eb9b2efc2007018'),
(27, 13, '86fbe8b737af36306f93236f49e5e3644394990ab45ea63b18be3a3e878019dcfa9b1dfdb8bfe0dab1cebbebf30bfa454a97e306592e77ff6a8739ac1b753dd0');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `users_keys`
--
ALTER TABLE `users_keys`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT для таблицы `users_keys`
--
ALTER TABLE `users_keys`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `users_keys`
--
ALTER TABLE `users_keys`
  ADD CONSTRAINT `users_keys_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
