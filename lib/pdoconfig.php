<?php 
	class PDOConfig extends PDO{
		private $engine;
		private $host;
		private $database;
		private $user;
		private $pass;
		private $connection;
		private $last_query;

		public function __construct()
		{
			$this->engine='mysql';
			$this->host='localhost';
			$this->database='reg';
			$this->user='root';
			$this->pass='';
			$this->setConnection();
		}

		//установление соединения c БД
		private function setConnection()
		{
			$dns=$this->engine.':dbname='.$this->database.';host='.$this->host;
			$this->connection=new PDO($dns, $this->user, $this->pass);
			$this->connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$this->connection?:die('Соединение с Базой Данных установить не удалось');
		}

		//получить соединение
		public function getConnection()
		{
			return $this->connection;
		}

		// Подготавливает запрос к выполнению и возвращает связанный с этим запросом объект
		private function prep_query($query)
		{
			return $this->connection->prepare($query);
		}

		//совершение запросов в бд
		public function executePrepare($query, $values=[])
		{
			$_POST['new']=$query;
			$this->last_query=$query;
			$stmt=$this->prep_query($query);
			$result=$stmt->execute($values);
			$this->confirm_query($result);
			return $stmt;
		}


		//Извлечение следующей строки из результирующего набора БД
		public function fetch($query, $values=null)
		{
			$stmt = $this->executePrepare($query, $values);
			return $stmt->fetch(PDO::FETCH_ASSOC);
		}

		//Возвращает массив, содержащий все строки результирующего набора
		public function fetchAll($query, $values=null, $key=null)
		{
			//$values=$this->doArray($values);
			$stmt = $this->executePrepare($query, $values);
			$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
			
			/*Позволяет пользователю получать результаты с использованием столбца из результатов в виде ключа для массива*/
			if($key != null && $results[0][$key]){
				$keyed_results = [];
				foreach($results as $result){
					$keyed_results[$result[$key]] = $result;
				}
				$results = $keyed_results;
			}
			return $results;
		}

		//проверка выполнения запроса
		public function confirm_query($result)
		{
			$result?:die('Не удалось выполнить запрос к базе данных'.$this->last_query);
		}
	}
 ?>